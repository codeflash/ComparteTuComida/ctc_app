package com.codeflash.compartetucomida.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.codeflash.compartetucomida.Fragments.ChatListFragment;
import com.codeflash.compartetucomida.Fragments.PlateNewOrEditFragment;
import com.codeflash.compartetucomida.Helpers.BottomNavigationViewHelper;
import com.codeflash.compartetucomida.Fragments.NotificationListFragment;
import com.codeflash.compartetucomida.Fragments.PlateListFragment;
import com.codeflash.compartetucomida.Fragments.ProfileFragment;
import com.codeflash.compartetucomida.R;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String STATE_FRAGMENT_POSITION = "stateFragmentPosition";
    private BottomNavigationViewEx navigation;
    private static final String STATE_BOTTOM_NAVIGATION = "stateBottomNavigation";
    private List<Fragment> fragments;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_plates:
                    switchFragment(0);
                    return true;
                case R.id.navigation_chat:
                    switchFragment(1);
                    return true;
                case R.id.navigation_add:
                    switchFragment(2);
                    return true;
                case R.id.navigation_notifications:
                    switchFragment(3);
                    return true;
                case R.id.navigation_me:
                    switchFragment(4);
                    return true;
            }
            return false;
        }

    };

    private void buildFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content , fragment)
                .commit();
    }

    private void switchFragment(int position){
        buildFragment(fragments.get(position));
    }

    private void loadFragments(){
        fragments = new ArrayList<>();
        fragments.add(new PlateListFragment());
        fragments.add(new ChatListFragment());
        fragments.add(new PlateNewOrEditFragment());
        fragments.add(new NotificationListFragment());
        fragments.add(new ProfileFragment());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadFragments();
        switchFragment(0);

        navigation = (BottomNavigationViewEx) findViewById(R.id.navigation);
        //navigation.setSelectedItemId(R.id.navigation_plates);
        navigation.setCurrentItem(0);
        navigation.setSelected(true);

        BottomNavigationViewHelper.setupBottomNavigationView(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save variables on screen orientation change. Save the user's current game state
        savedInstanceState.putInt(STATE_BOTTOM_NAVIGATION, navigation.getCurrentItem());
        savedInstanceState.putInt(STATE_FRAGMENT_POSITION, navigation.getCurrentItem());
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState);

        // Restore variables on screen orientation change. Restore state members from saved instance
        navigation.setCurrentItem(savedInstanceState.getInt(STATE_BOTTOM_NAVIGATION));
        Toast.makeText(this ,savedInstanceState.getInt(STATE_FRAGMENT_POSITION) + "" , Toast.LENGTH_SHORT ).show();
        switchFragment(savedInstanceState.getInt(STATE_FRAGMENT_POSITION) );
    }
}
