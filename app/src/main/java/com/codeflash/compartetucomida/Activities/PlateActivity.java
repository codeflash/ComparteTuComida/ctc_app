package com.codeflash.compartetucomida.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.codeflash.compartetucomida.CustomViews.NumberPickerView;
import com.codeflash.compartetucomida.R;

public class PlateActivity extends AppCompatActivity {

    private static final String STATE_NUMBER_PICKER_VALUE = "numberPickerValue";
    private NumberPickerView numberPickerView;
    private Button plateOrderButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plate);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        numberPickerView = (NumberPickerView) findViewById(R.id.numberPicker);

        plateOrderButton = (Button) findViewById(R.id.plate_order);

        numberPickerView.setMaxValue(10);
        numberPickerView.setMinValue(1);
        numberPickerView.setValue(1);


        plateOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PlateActivity.this , "El valor del picker es: " + String.valueOf(numberPickerView.getValue()) , Toast.LENGTH_SHORT).show();
            }
        });
        setSupportActionBar(toolbar);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save variables on screen orientation change. Save the user's current game state
        savedInstanceState.putInt(STATE_NUMBER_PICKER_VALUE, numberPickerView.getValue());
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState);

        // Restore variables on screen orientation change. Restore state members from saved instance
       numberPickerView.setValue(savedInstanceState.getInt(STATE_NUMBER_PICKER_VALUE));
    }

}
