package com.codeflash.compartetucomida.CustomViews;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codeflash.compartetucomida.R;

/**
 * Created by Luigi on 12/06/2017.
 */

public class NumberPickerView extends LinearLayout {
    private int minValue = 0;
    private int maxValue = 999999999;
    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public int getValue() {
        return Integer.valueOf(txtNumber.getText().toString());
    }

    public void setValue(int newValue) {
        int value = newValue;
        if(newValue < minValue) {
            value = minValue;
        } else if (newValue > maxValue) {
            value = maxValue;
        }

        txtNumber.setText(String.valueOf(value));
    }


    private View rootView;
    private Button btnPlus, btnMinus;
    private TextView txtNumber;

    public NumberPickerView(Context context) {
        super(context);
        init(context);
    }
    public NumberPickerView(Context context , AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }
    private void init(Context context) {
        setSaveEnabled(false);
        rootView = inflate(context, R.layout.number_picker, this);
        txtNumber = (TextView) rootView.findViewById(R.id.txtNumber);

        btnMinus = (Button) rootView.findViewById(R.id.btn_minus);
        btnPlus = (Button) rootView.findViewById(R.id.btn_plus);

        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decrementValue(); //we'll define this method later
            }
        });

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incrementValue(); //we'll define this method later
            }
        });
    }

    private void incrementValue() {
        int currentVal = Integer.valueOf(txtNumber.getText().toString());
        if(currentVal < maxValue) {
            txtNumber.setText(String.valueOf(currentVal + 1));
        }
    }

    private void decrementValue() {
        int currentVal = Integer.valueOf(txtNumber.getText().toString());
        if(currentVal > minValue) {
            txtNumber.setText(String.valueOf(currentVal - 1));
        }
    }



}



