package com.codeflash.compartetucomida.Fragments;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codeflash.compartetucomida.R;
import com.codeflash.compartetucomida.dummy.DummyContent;

import java.util.List;

/**
 * Created by Luigi on 9/06/2017.
 */

public class ChatListRecyclerViewAdapter extends RecyclerView.Adapter<ChatListRecyclerViewAdapter.ViewHolder> {

    private final List<DummyContent.DummyItem> mValues;

    private  OnItemClickListener mListener;
    public interface OnItemClickListener{
        void onClick();
    }


    ChatListRecyclerViewAdapter(List<DummyContent.DummyItem> items , OnItemClickListener listener ) {
        mValues = items;
        mListener = listener;

    }





    @Override
    public ChatListRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat, parent, false);
        return new ChatListRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ChatListRecyclerViewAdapter.ViewHolder holder, int position) {
        //holder.mBadge.setText(spannableString);
        holder.mItem = mValues.get(position);
        holder.mPlateView.setText("Lomo Saltado");
        holder.mAuthorView.setText("Melisa Cardenas");
        holder.mMessageView.setText("asdsad asd asd sad  asd asd asd as d asd asd asd asd as da sd asd asd as das dsa asdsad asd asd sad  asd asd asd as d asd asd asd asd as da sd asd asd as das dsa");
        //holder.mTitleView.setText(mValues.get(position).id);


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onClick();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        //public final TextView mTitleView;


        //public final TextView mPorcionsView;

        public DummyContent.DummyItem mItem;
        public ImageView mImageView;
        public TextView mAuthorView;
        public TextView mPlateView;
        public TextView mMessageView;
        //public TextView mBadge;


        public ViewHolder(View view) {
            super(view);
            mView = view;

            //mTitleView = (TextView) view.findViewById(R.id.plate_title);
            mPlateView = (TextView) view.findViewById(R.id.chat_plate);
            mImageView = (ImageView) view.findViewById(R.id.chat_image);
            mAuthorView = (TextView) view.findViewById(R.id.chat_author);
            mMessageView = (TextView) view.findViewById(R.id.chat_message);
            //mPorcionsView = (TextView) view.findViewById(R.id.plate_porcions);
            //mBadge = (TextView) view.findViewById(R.id.plate_rate);



        }

        /*@Override
        public String toString() {
            return super.toString() + " '" + mTitleView.getText() + "'";
        }*/
    }


}
