package com.codeflash.compartetucomida.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.codeflash.compartetucomida.Activities.NotificationActivity;
import com.codeflash.compartetucomida.Activities.PlateActivity;
import com.codeflash.compartetucomida.R;
import com.codeflash.compartetucomida.dummy.DummyContent;
import com.codeflash.compartetucomida.dummy.DummyContent.DummyItem;


public class PlateListFragment extends Fragment implements PlateListRecyclerViewAdapter.OnItemClickListener, Toolbar.OnMenuItemClickListener {




    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PlateListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PlateListFragment newInstance(int columnCount) {
        PlateListFragment fragment = new PlateListFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_plate_list, container, false);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.menu_plate);
        toolbar.setNavigationIcon(R.drawable.ic_notifications_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PlateListFragment.this.getActivity() , NotificationActivity.class);
                startActivity(i);
            }
        });

        toolbar.setOnMenuItemClickListener(this);
        PlateListRecyclerViewAdapter adapter = new PlateListRecyclerViewAdapter(DummyContent.ITEMS);
        adapter.setOnClickItemListener(this);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list_plate);
        recyclerView.setAdapter(adapter);

        return view;
    }


    @Override
    public void onClick() {
        Intent i = new Intent(this.getActivity() , PlateActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent i = new Intent(this.getActivity() , NotificationActivity.class);
                startActivity(i);
                return true;
        }
        return false;
    }


}
