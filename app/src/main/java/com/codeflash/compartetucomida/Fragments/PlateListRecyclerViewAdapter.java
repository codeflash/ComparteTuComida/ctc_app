package com.codeflash.compartetucomida.Fragments;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.codeflash.compartetucomida.R;
import com.codeflash.compartetucomida.dummy.DummyContent.DummyItem;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link PlateListFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class PlateListRecyclerViewAdapter extends RecyclerView.Adapter<PlateListRecyclerViewAdapter.ViewHolder> {

    private final List<DummyItem> mValues;

    private int type;
    public static int CARDS = 1;
    public static int LIST = 2;

    private  OnItemClickListener mListener;

    public void setOnClickItemListener(OnItemClickListener mListener) {
        this.mListener = mListener;
    }



    public interface OnItemClickListener{
        void onClick();
    }

    public PlateListRecyclerViewAdapter(List<DummyItem> items ) {
        mValues = items;
        type = CARDS;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if(type == CARDS){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_plate_card, parent, false);
        }else if(type == LIST){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_plate_list, parent, false);
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        //holder.mBadge.setText(spannableString);
        holder.mItem = mValues.get(position);
        //holder.mTitleView.setText(mValues.get(position).id);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onClick();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        //public final TextView mTitleView;


        //public final TextView mPorcionsView;

        public DummyItem mItem;
        public ImageView mImageView;
        //public TextView mBadge;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            if(type == CARDS){
                mImageView = (ImageView) view.findViewById(R.id.plate_image);
            }else if(type == LIST) {

            }

                //mTitleView = (TextView) view.findViewById(R.id.plate_title);

            //mPorcionsView = (TextView) view.findViewById(R.id.plate_porcions);
            //mBadge = (TextView) view.findViewById(R.id.plate_rate);



        }


    }


}
