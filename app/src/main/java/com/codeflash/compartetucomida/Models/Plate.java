package com.codeflash.compartetucomida.Models;

/**
 * Created by Luigi on 12/06/2017.
 */

public class Plate {
    private String title;
    private String description;
    private Float price;

    public Plate(String title, String description, Float price) {
        this.title = title;
        this.description = description;
        this.price = price;
    }

    public Plate() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }


}
